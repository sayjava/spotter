- POC project
- No test but should be easy to retrofit
- I opted for using RxJS for my state management than using the Redux pattern but still keeping to `action -> store -> render` pattern
- The main component at `src/pages/Home` is just a dump ground for testing the components for now until I flesh out the pages for the applications. 

### State Management (Motivations) (src/stores)
- Stores are small encapsulated *state + actions* implemented using ReplaySubjects see `src/stores/app`

- Multiple stores can be composed to generate a *store-of-stores* if a component needs multiple sources of data without directly dealing 
with multiple stores see src/pages/Home.js usage of Save. see `src/utils/compose`

-  `src/components/Observer` is a wrapper component that monitors changes on a store (or store-of-stores) and render its child component passing the store as props. This is a more reactive approach to building components and it comes with some benefits
    - The logic of listening to stores and re-rendering the view is kept in a single component that be tested and dusted
    - The encapsulated component is shielded from the implementation of the stores
    - Easier and small components to test
    - The observer can monitor parts of the store that has change and re-render accordingly.


### RxJS -> Reactive + Functional (Multiple dependent asynchronous actions)
- See `src/stores/app#save` for an example of asynchronous actions that depend on each other to succeed. RxJS makes it 
easy to map, filter and reduce tasks.

- See `src/lib/uploads` for the use of a progress subject