import React from 'react'

export const Save = (props) => {
    let { user, place, image, onReadyToSave } = props
    let readyToSave = user && place && image
    return (
        <div>
            {readyToSave ? <div>
                <button className='uk-button' onClick={() => onReadyToSave({ user, place, image })}>Save</button>
            </div> : <span>Not Ready</span>}
        </div>
    )

}