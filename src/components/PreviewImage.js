import React from 'react'
import { uploadImage } from '../lib/upload'

export class PreviewImage extends React.Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.image) {
            let reader = new FileReader();
            reader.onloadend = (evt) => {
                if (evt.target.readyState === FileReader.DONE) {
                    this.setState({
                        imageData: evt.target.result
                    })
                }
            }
            reader.readAsDataURL(nextProps.image);
        }
    }

    render() {
        let { image } = this.props
        let { imageData } = this.state || {}
        let imgStyle = {
            width: `400px`,
            height: `400px`,
            margin: 'auto'
        }
        return (
            <div className='' style={imgStyle}>
                {
                    imageData ? <div>
                        <div className="uk-background-cover uk-card uk-card-default uk-card-body uk-border-rounded">
                            <img src={imageData} style={imgStyle} />
                        </div>
                    </div> : null
                }
            </div>
        )
    }
}
