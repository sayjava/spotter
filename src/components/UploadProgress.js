import React from 'react'

export class UploadProgress extends React.Component {

    constructor() {
        super()
        this.state = {
            progress: 0,
            completed: false
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            progress: 0,
            completed: false
        })

        let subscription = props.uploadTask
            .subscribe(
            (progress) => this.setState({ progress }),
            (error) => this.setState({ error }),
            () => {
                subscription.dispose()
                this.setState({ completed: true })
            });
    }

    render() {
        let { progress, completed } = this.state

        let progressStyle = {
            width: (Number.isInteger(progress) ? `${progress}%` : '100%')
        }

        return (
            <div>
                {completed ? <div>Upload Completed {progress}</div> : null}
                {progress > 0 && progress < 100 ? <div className='bar bar-sm'>
                    <div className='bar-item' role='progressbar' style={progressStyle} ></div>
                    Progress {progress}%
                </div> : null}
            </div>
        )
    }
}