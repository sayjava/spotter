import React from 'react'
import { places } from '../lib/location'

export class Restaurants extends React.Component {

    constructor() {
        super()
        this.state = {
            places: []
        }

        this.map = null
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            places: []
        })
        places(this.map, nextProps.location).then((restaurants) => this.setState({ places: restaurants }))
    }

    renderMap(dom) {
        if (!this.map) {
            this.map = new window.google.maps.Map(dom, {
                zoom: 15
            });
        }
    }

    renderPlaces(places) {
        return places.splice(0, 10).map((place) => {
            return (
                <li key={place.id} onClick={() => this.props.onSelected(place)}>
                    {place.name}
                </li>
            )
        })
    }

    parseVicinity(places) {
        if (places.length > 0) {
            let [{ vicinity }] = places
            return vicinity
        }

        return ''
    }

    render() {
        let { places } = this.state
        return (
            <div className='uk-panel'>
                <div className='Map' ref={(dom) => this.renderMap(dom)}></div>
                <div>
                    <h4>{this.parseVicinity(places)}</h4>
                    <ul className='uk-list uk-list-striped'>
                        {this.renderPlaces(places)}
                    </ul>
                </div>
            </div>
        )
    }
}