import React from 'react'

const Profile = (props) => {
    let { user } = props
    let [firstName] = user.displayName.split(' ')
    return (
        <div className='uk-panel'>
            <span>Hello {firstName}</span>
        </div>
    )
}

export const User = ({ user, login }) => {
    return (
        <div className='uk-padding'>
            {user ? <Profile user={user} /> : <div>
                <button onClick={e => login()}>Login</button>
            </div>}
        </div>
    )
}