import React from 'react'

export const ImageChooser = (props) => {
    return (
        <div>
            <input type="file" accept="image/*" onChange={e => props.onChange(e.target.files[0])} />
        </div>
    )
}