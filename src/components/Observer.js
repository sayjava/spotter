
import React from 'react'
/**
 *  A wrapper component that wraps other components with a store and renders 
 *  the inner component when the store changes
 */
export class Observer extends React.Component {

    componentDidMount() {
        let { store, watch } = this.props

        // subscribe to the events of the store and check if the propert has changed
        this.subscription = this.props.store.subscribe((store) => {

            if (watch) {
                let watchedValue = store[watch]
                // if a specific variable is watched, only render if it changes
                if (this.currentWatchValue !== watchedValue) {
                    this.currentWatchValue = watchedValue
                    this.setState(store)
                }
            } else {
                // if nothing in particulary watched
                this.setState(store)
            }

        })
    }

    componentWillUnMound() {
        // derigister from the madness
        this.subscription.unsubscribe()
    }

    render() {
        let props = this.state || {}
        return React.cloneElement(React.Children.only(this.props.children), props)
    }

}