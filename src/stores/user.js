import { auth } from '../lib/firebase'
import * as Rx from 'rx-lite'

export const SIGNED_OUT = "signed_out"
export const NOT_SIGNED_IN = "not_signed_in"

class AuthStore extends Rx.ReplaySubject {

    constructor(params) {
        super(params)
        this.initAuth()
    }

    initAuth() {
        auth().onAuthStateChanged((result) => {
            if (result) {
                if (result.isAnonymous) {
                    this.onNext({ error: result, message: 'Anonymouse User', code: NOT_SIGNED_IN })
                } else {
                    this.onNext({
                        user: result
                    })
                }
            } else {
                this.onNext({
                    error: result, message: 'anonymous', code: SIGNED_OUT
                })
            }
        })
    }

    doAuth() {
        auth().signInWithPopup(new auth.GoogleAuthProvider())
            .then((result) => this.onNext(result.user))
            .catch((error) => this.onError({ error, code: NOT_SIGNED_IN }))
        return this
    }

}

export const UserStore = AuthStore