import { process as processExif } from '../lib/exif'
import { ReplaySubject } from 'rx-lite'

import { uploadImage } from '../lib/upload'
import { save as saveReview } from '../lib/review'

class Store extends ReplaySubject {

    constructor(args) {
        super(args)
        this.update()
    }

    update() {
        this.onNext(Object.assign({}, this))
    }

    setImage(image) {
        // TODO: check if the image is a food
        this.image = image
        this.update()

        // TODO: fetch vision labels
        processExif(image).then((gps) => {
            this.location = gps
            this.update()
        }, () => {
            // no gps info found in the image, ask the user for location
            navigator.geolocation.getCurrentPosition((position) => {
                const { coords } = position
                this.location = coords
                this.update()
            });
        })
    }

    setPlace(place) {
        this.place = place
        this.update()
    }

    save({ user }) {
        const doSaveReview = (place, image) => {
            let userJSON = user.toJSON()
            return saveReview({
                user: {
                    id: userJSON.uid,
                    name: userJSON.displayName
                },
                place,
                image,
            })
        }

        // start uploading the image
        this.uploadTask = uploadImage(this.image)
        this.update()

        // TODO clean up the subscriptions
        //Mapp the upload task to a save task
        this.saveTask = this.uploadTask
            .filter(p => Number.isNaN(Number(p))) // filter out non number updates, effectively only waiting for the image url 
            .map(doSaveReview.bind(this, this.place.id))
            .switch()
        this.update()

        return this.saveTask.subscribe((r) => console.log('REVIEW SAVED ----- > ', r))
    }

}

export const AppStore = Store

