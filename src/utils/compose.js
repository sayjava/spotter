import { Observable } from 'rx-lite'

export const composeStores = (...stores) => {
    return Observable.combineLatest(stores)
        .map((values) => Object.assign({}, ...values))
}