
const PLACES_KEY = 'AIzaSyDl9GzaFuJROfPmfQtFLwQHc9sFYXvnvjM'
const google = window.google

const findRestaurants = (location, map) => {

    return new Promise((resolve, reject) => {

        const request = {
            location,
            type: 'restaurant',
            rankBy: google.maps.places.RankBy.DISTANCE,
        };

        const service = new google.maps.places.PlacesService(map);

        service.nearbySearch(request, (result, status) => {

            if (status == google.maps.places.PlacesServiceStatus.OK) {
                console.log(JSON.stringify(result))
                resolve(result)
            } else {
                reject(status)
            }
        });

    })

}

export const places = (map, coords) => {

    const currentLocation = new google.maps.LatLng(coords.latitude, coords.longitude)

    return Promise.resolve(require('../data/places.json'))
    //return findRestaurants(currentLocation, map)
}