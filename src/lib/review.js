
import { database } from './firebase'
import { Observable } from 'rx-lite'

export const save = (options) => {
    options.created = Date.now()
    return Observable.fromPromise(database().ref(`places/${options.place}/reviews`).push(options))
}   