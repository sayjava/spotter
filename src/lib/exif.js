const ExifParser = require('exif-parser')

export const process = (image) => {

    return new Promise((resolve, reject) => {

        const reader = new FileReader(image)

        reader.onloadend = (evt) => {
            if (evt.target.readyState === FileReader.DONE) {

                let exif = ExifParser.create(evt.target.result).parse()

                if (exif.tags && exif.tags.GPSDateStamp) {
                    resolve({
                        latitude: exif.tags.GPSLatitude,
                        longitude: exif.tags.GPSLongitude
                    })
                } else {
                    reject(exif.tags)
                }
            }
        }

        reader.readAsArrayBuffer(image)
    })
}