
import { storage } from './firebase'
import * as Rx from 'rx-lite'

export const uploadImage = (file) => {
    const path = `images/${file.name}`
    const storageRef = storage().ref().child(path)
    const reader = new FileReader()
    const subject = new Rx.ReplaySubject()

    reader.onloadend = (evt) => {
        const doUpload = () => {
            let uploadTask = storageRef.put(file, { contentType: 'image/jpeg' })

            uploadTask.on('state_changed', (snapshot) => {
                const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
                switch (snapshot.state) {
                    case storage.TaskState.PAUSED:
                        break;
                    case storage.TaskState.RUNNING:
                        subject.onNext(progress)
                        break;
                }
            }, (error) => {
                subject.onError(error)
            }, () => {
                subject.onNext(uploadTask.snapshot.downloadURL)
                subject.onCompleted()
            })
        }

        //TODO : Remove 
        const simulateUpload = () => {
            [100, 200, 300, 400, 500, 600, 700, 1000, 'THE IMAGE URL'].forEach((x, index) => {
                setTimeout(() => {
                    subject.onNext(Number.isNaN(x / 10) ? 'http://stickimage.jpg' : x / 10)
                }, 400 * index)
            })

            setTimeout(subject.onCompleted.bind(subject), 10000)
        }

        if (evt.target.readyState === FileReader.DONE) {
            doUpload()
        }
    }

    reader.readAsDataURL(file)

    return subject
}
