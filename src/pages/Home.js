import React, { Component } from 'react';

import { ImageChooser } from '../components/ImageChooser'
import { PreviewImage } from '../components/PreviewImage'
import { Restaurants } from '../components/Places'
import { UploadProgress } from '../components/UploadProgress'
import { User } from '../components/User'
import { Save } from '../components/Save'
import { Observer } from '../components/Observer'

// utils
import { composeStores } from '../utils/compose'

// stores
import { AppStore } from '../stores/app'
import { UserStore } from '../stores/user'

class Home extends Component {

  constructor() {
    super()

    // initialise the stores
    this.userStore = new UserStore()
    this.appStore = new AppStore()
  }

  render() {

    return (
      <div className="container">

        <Observer store={this.userStore} watch='user'>
          <User />
        </Observer>

        <div>
          <ImageChooser onChange={(image) => this.appStore.setImage(image)} />
        </div>

        <div>
          <Observer store={this.appStore} watch="image">
            <PreviewImage />
          </Observer>
        </div>

        <Observer store={this.appStore} watch="location">
          <Restaurants onSelected={(place) => this.appStore.setPlace(place)} />
        </Observer>

        <Observer store={this.appStore} watch="uploadTask">
          <UploadProgress />
        </Observer>

        <Observer store={composeStores(this.userStore, this.appStore)} >
          <Save onReadyToSave={(options) => this.appStore.save(options)} />
        </Observer>

      </div>
    );
  }
}

export default Home;
